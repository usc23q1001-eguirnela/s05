# [SECTION] Python Class Review
class SampleClass():
	def __init__(self, year):
		self.year = year

	def showYear(self):
		print(f"The year is: {self.year}")

myObj = SampleClass(2023)
print(myObj.year)
myObj.showYear()

# [SECTION] Fundamentals of OOP
# 4 Main Fundamental principles of OOP: Encapsulation, Inheritance, Polymorhpism, and Abstraction


# [SECTION] Encapsulation
# is a mechanism of wrapping the attributes and codes acting on the methods together as a single unit.
# AKA data hiding

# the prefix underscore(_) is used as a warning for developers that means: please be careful about this attribute or method, dont use it outside the declared class
class Person():
	def __init__(self):
		# protected attribute _name
		self._name = "Jah'rakal"
		self._age = 21

	#Setters&Getters
	def setName(self, name):
		self._name = name

	def getName(self):
		print(f"Name of Person: {self._name}")

	def setAge(self, age):
		self._age = age

	def getAge(self):
		print(f"Age of Person: {self._age}")

p1 = Person()
print(p1._name)
p1.getName()
p1.setName("Khan")
p1.getName()
p1.getAge()
p1.setAge(40)
p1.getAge()


# [SECTION] Inheritance
# The transfer of characteristics of a parent class to the child classes
# parent-child relationship
# To create an inherited class, in the className definition we add the parent class as the parameter of the child class
# Syntax: class className(Parent):

class Employee(Person):
	def __init__(self, employeeID):
		# super() can be used to invoke the immediate parent class constructor
		super().__init__()
		# unique attribute to the Employee class
		self._employeeID = employeeID

	#Setters&Getters 
	def setEmployeeID(self, employeeID):
		self._employeeID = employeeID

	def getEmployeeID(self):
		print(f"ID of Employee: {self._employeeID}")

	def getDetails(self):
		print(f"{self._employeeID} belongs to {self._name}")

emp1 = Employee("23001")
emp1.getDetails()
emp1.getName()
emp1.getAge()
emp1.setName("Krobelus")
emp1.setAge(500)
emp1.getDetails()

class Student(Person):
	def __init__(self, studentID, course, yearLevel):
		super().__init__()
		self._studentID = studentID
		self._course = course
		self._yearLevel = yearLevel

	#Setters&Getters 
	def setStudentID(self, studentID):
		self._studentID = studentID

	def getStudentID(self):
		print(f"ID of Student: {self._studentID}")

	def setCourse(self, course):
		self._course = course

	def getCourse(self):
		print(f"Course of Student: {self._course}")

	def setYearLevel(self, yearLevel):
		self._yearLevel = yearLevel

	def getYearLevel(self):
		print(f"Year Level of Student: {self._yearLevel}")

	def getDetails(self):
		print(f"{self._name} is currently in year {self._yearLevel} taking up {self._course}")

stud1 = Student("17100409", "BSCS", "4")
stud1.getDetails()


# [SECTION] Polymorhpism
# from greek word "many forms"
# The method inherited from the parent class is not always fit for the child class. Re-implementation/Overriding of method can be done in the child class.

# There are different methods to use polymorhpism in Python.

# 1. Funtion and Objects
# a function can be created that can take any object, allowing polymorhpism
class Admin():
	def isAdmin(self):
		print(True)

	def userType(self):
		print("Admin User")

class Customer():
	def isAdmin(self):
		print(False)

	def userType(self):
		print("Customer User")

# define a test function that will take an object called obj
def testFunction(obj):
	obj.isAdmin()
	obj.userType()

# create object instance of Admin and Customer
userAdmin = Admin()
userCustomer = Customer()

# pass the created instance to test function
testFunction(userAdmin)
testFunction(userCustomer)


# 2. Polymorhpism with Class methods
# Python uses two different class types in the same way.
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()


# 3. Polymorhpism with Inheritance
# polymorhpism in Python defines methods in child class that have the same name as mehtods in the parent class. "Method Overriding"

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks(developer career, pi-shape career, and short courses)")

	def numOfHours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	# override
	def numOfHours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
	# override
	def numOfHours(self):
		print("Learn skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# override
	def numOfHours(self):
		print("Learn advanced topics in web development in 20 hours!")

course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.numOfHours()
course2.numOfHours()
course3.numOfHours()


# [SECTION] Abstraction
# abstract class can be considered as a blueprint for the other class.

# Abstract Base Classes (abc) - the import tells the program to get the abc module of Python to be used
from abc import ABC, abstractclassmethod

class Polygon(ABC):
	@abstractclassmethod
	def displayNumberOfSides(self):
		# this denotes that the method doesn't do anyting
		pass


class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def displayNumberOfSides(self):
		print("Triangle has 3 sides.")

class Quadrilateral(Polygon):
	def __init__(self):
		super().__init__()

	def displayNumberOfSides(self):
		print("Quadrilateral has 4 sides.")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def displayNumberOfSides(self):
		print("Pentagon has 5 sides.")


shape1 = Triangle()
shape2 = Quadrilateral()
shape3 = Pentagon()

shape1.displayNumberOfSides()
shape2.displayNumberOfSides()
shape3.displayNumberOfSides()