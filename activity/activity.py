from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# Setters
	def setName(self, name):
		self._name = name

	def setBreed(self, breed):
		self._breed = breed

	def setAge(self, age):
		self._age = age

	# Getters
	def getName(self):
		print(f"The name of the dog is {self._name}")

	def getBreed(self):
		print(f"The breed of the dog is {self._breed}")

	def getAge(self):
		print(f"The age of the dog is {self._age}")

	# Functions
	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print("Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self._name}!")

class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# Setters
	def setName(self, name):
		self._name = name

	def setBreed(self, breed):
		self._breed = breed

	def setAge(self, age):
		self._age = age

	# Getters
	def getName(self):
		print(f"The name of the cat is {self._name}")

	def getBreed(self):
		print(f"The breed of the cat is {self._breed}")

	def getAge(self):
		print(f"The age of the cat is {self._age}")

	# Functions
	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self._name}, come on!")

# Test
dog1 = Dog("Raizor", "Bulldog", 10)
dog1.eat("Meat")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puck", "Birman", 6)
cat1.eat("Fish")
cat1.make_sound()
cat1.call()